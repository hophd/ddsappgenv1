#ifndef __HIGHERFUNC_H__
  #define __HIGHERFUNC_H__

string getInformation(char *topic_name);
void sendMessage(char *cmd, char *topic_name);

#endif
